package com.sda.geotrif.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;

public class FileUtil {

	public static File readFile() {
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter the filename:");
		String fileName = input.next();

		File file = new File(
				"C:\\Users\\GeoTrif\\Desktop\\Java_Advance_SDA_WorkSpace\\RealEstateOffice\\src\\" + fileName + ".txt");

		return file;
	}

	public static void createAgentReportFile() throws IOException {
		File file = new File(
				"C:\\Users\\GeoTrif\\Desktop\\Java_Advance_SDA_WorkSpace\\RealEstateOffice\\src\\agentReport.txt");
		if (!file.exists()) {
			file.createNewFile();
		} else {
			System.out.println("File allready exists.");
		}
	}

	public static void createOverviewFile() throws IOException {
		File file = new File(
				"C:\\Users\\GeoTrif\\Desktop\\Java_Advance_SDA_WorkSpace\\RealEstateOffice\\src\\overview.txt");
		if (!file.exists()) {
			file.createNewFile();
		} else {
			System.out.println("File allready exists.");
		}
	}

	public static String getAgentReportFileName() {
		return "C:\\Users\\GeoTrif\\Desktop\\Java_Advance_SDA_WorkSpace\\RealEstateOffice\\src\\agentReport.txt";
	}

	public static String getOverviewFileName() {
		return "C:\\Users\\GeoTrif\\Desktop\\Java_Advance_SDA_WorkSpace\\RealEstateOffice\\src\\overview.txt";
	}

	public static void writeToFile(Set<String> mySet, String fileName, Map<Integer, Double> map) {
		Iterator iterator = mySet.iterator();
		Iterator<Entry<Integer, Double>> mapIterator = map.entrySet().iterator();

		try {
			PrintWriter report = new PrintWriter(fileName);

			while (iterator.hasNext()) {
				report.println(iterator.next() + " ");
				report.flush();
			}

			report.println();

			while (mapIterator.hasNext()) {

				report.println(mapIterator.next() + " ");
				report.flush();
			}

			System.out.println("Writing success.");
			report.close();
		} catch (IOException e) {
			System.out.println("Error could not write to the location.");
		}
	}

}
