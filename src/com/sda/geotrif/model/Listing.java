package com.sda.geotrif.model;

public class Listing {

	private String propertyNumber;
	private String propertyType;
	private double price;
	private int agentId;

	public Listing() {

	}

	public Listing(String propertyNumber, String propertyType, double price, int agentId) {
		this.propertyNumber = propertyNumber;
		this.propertyType = propertyType;
		this.price = price;
		this.agentId = agentId;
	}

	public String getPropertyNumber() {
		return this.propertyNumber;
	}

	public String getPropertyType() {
		return this.propertyType;
	}

	public double getPrice() {
		return this.price;
	}

	public int getAgentId() {
		return this.agentId;
	}

}
