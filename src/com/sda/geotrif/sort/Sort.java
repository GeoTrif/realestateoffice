package com.sda.geotrif.sort;

import java.util.Comparator;
import java.util.List;

import com.sda.geotrif.Overview;

public final class Sort<T extends Comparable<? super T>> implements Comparator<T> {

	@Override
	public final int compare(T o1, T o2) {

		return o1.compareTo(o2);
	}
	
	

}

class SortImp implements Comparator<String>{

	@Override
	public int compare(String o1, String o2) {
		
		return Integer.valueOf(o2.compareToIgnoreCase(o1));
	}
	
}
