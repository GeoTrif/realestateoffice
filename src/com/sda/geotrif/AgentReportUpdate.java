package com.sda.geotrif;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.sda.geotrif.model.Listing;

public class AgentReportUpdate {

	public static void main(String[] args) throws FileNotFoundException {

		Set<String> mySet = new TreeSet<>();
		Map<Integer, Double> map = new TreeMap<>();

		Scanner input = new Scanner(System.in);
		System.out.println("Please enter the filename:");
		String fileName = input.next();

		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		while (sc.hasNextLine()) {

			String line = sc.nextLine();
			mySet.add(line.split(" ")[1].toUpperCase());

			Listing listing = new Listing(line.split(" ")[0], line.split(" ")[1],
					Double.parseDouble(line.split(" ")[2]), Integer.parseInt(line.split(" ")[3]));

			map.put(listing.getAgentId(), listing.getPrice());

		}

		for (Integer keymap : map.keySet()) {
			System.out.println(keymap + " = " + map.get(keymap));
		}

		System.out.println("*************************");

		System.out.println(mySet);

	}
}


