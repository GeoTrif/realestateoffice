package com.sda.geotrif;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/*
 * Your client is a real estate office manager who needs to run reports on property listings. You will build two applications that will provide two 
different reports. One report is focused on the amount of property an agent is selling. The other report provides an overview of the types of property 
for sale.
Your listings file will contain property number, property type, price, and agent id.
Task:
A.  Develop an application that reads your listings.txt file, analyzes the property listed per agent, and outputs a report to an agentreport.txt file. 
Your application should do the following:
1.  Prompt the user for the name of the input file (listings.txt).
2.  Use an unbuffered file input stream to open listings.txt file and read in property listings.
3.  Store each property type into a Set.
a.  Convert property type to upper case before adding to your Set using method(s) from String class.
b.  Sort your Set of property types alphabetically.
4.  Use a Map to calculate total property listed in dollars and cents for each agent id.
Note: Agent id would be the key, and accumulated total of property listed would be the value.
4.1  Sort your Map by agent id.
4.2  Create an agentreport.txt file.
5.  Use an Iterator to iterate through your Set and write your sorted set of property types sold by the agents to the agentreport.txt file using 
unbuffered file output stream.
6.  Iterate through your Map to write your sorted pair of agent id and total property listed to the agentreport.txt file using unbuffered file output
 stream. 
B.  Develop an application that reads your listings.txt file, analyzes the properties listed, and outputs an overview of properties listed to an 
overview.txt file. Your application should do the following:
1.  Prompt the user for the name of the input file (listings.txt).
2.  Open the listings.txt file and read in property listing information using a buffered FileReader.
3.  Count the total number of property listings for sale.
3.1 Use buffered FileWriter to write the count of the number of property listings to your overview.txt file.
4.  Calculate the total value of property for sale.
4.1 Use a buffered FileWriter to write the total value of properties currently for sale.
5.  Store each property id into an ArrayList.
a.  Sort the ArrayList of property ids using natural ordering.
b.  Use a for-each loop to iterate through the sorted ArrayList and write property ids to overview.txt file using buffered FileWriter.
6.  Use buffered FileWriter to write the total value of the properties listed and the total number of properties currently for sale.
 */

public class AgentReport {

	private static String propertyNumber;
	private static String propertyType;
	private static double price;
	private static int agentId;

	public static void main(String[] args) throws FileNotFoundException {

		Set<String> set = new HashSet<>();
		String token1 = "";

		Set<String> set1 = new TreeSet<>();
		Map<Integer, Double> map = new HashMap<>();

		Scanner input = new Scanner(System.in);
		System.out.println("Please enter the filename:");
		String fileName = input.next();

		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		while (sc.hasNextLine()) {
			// token1 = sc.next(" ");
			token1 = sc.nextLine(); // sa scoatem string-urile si sa punem o conditie daca este numar sau nu
			set.add(token1);

		}

		sc.close();

		System.out.println(set);

		String[] str1 = (String[]) set.toArray(new String[set.size()]);

		System.out.println(Arrays.toString(str1));

		for (String sr : str1) {
			String[] s2 = sr.split(",");
			for (String result : s2) {

				String[] s3 = result.split(" ");

				for (int i = 0; i < s3.length - 1; i++) {

					propertyNumber = s3[0];
					propertyType = s3[1];
					price = Double.parseDouble(s3[2]);
					agentId = Integer.parseInt(s3[3]);

					System.out.print(propertyNumber + " ");
					System.out.print(propertyType + " ");
					System.out.print(price + " ");
					System.out.println(agentId + " ");
					System.out.println();

					set1.add(propertyType.toUpperCase());

					map.put(agentId, price);

					break;

				}

			}
		}

		System.out.println("---------------------------------");

		for (String s : set1) {
			System.out.print(s + " ");
		}

		System.out.println("\n------------------");

		for (Integer keyMap : map.keySet()) {
			System.out.println(keyMap + " " + map.get(keyMap));
		}

	}

}
