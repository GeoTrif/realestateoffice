package com.sda.geotrif;

import java.io.*;
import java.util.*;

import com.sda.geotrif.sort.Sort;
import com.sda.geotrif.util.FileUtil;

public class Overview {

	public static int countProperties;

	public static void main(String[] args) throws Exception {
		File file = FileUtil.readFile();

		List<Double> priceList = new ArrayList<>();
		List<Long> propertyIdList = new ArrayList<>();

		readInData(file, priceList, propertyIdList);

		System.out.println("The number of properties in the listing = " + countProperties);

		FileUtil.createOverviewFile();

		String fileName = FileUtil.getOverviewFileName();

		writeInData(fileName, "\nTotal number of properties: " + Integer.toString(countProperties));

		System.out.println("Total value of properties listed = " + calculateTotalValue(priceList));

		writeInData(fileName, "\nTotal value of properties listed: " + Double.toString(calculateTotalValue(priceList)));

		System.out.println("Before sorting:");
		for (long propertyId : propertyIdList) {
			System.out.println(propertyId);
		}

		System.out.println("After sorting:");

		Collections.sort(propertyIdList);

		for (long propertyId : propertyIdList) {
			System.out.println(propertyId);
		}

		for (long propertyId : propertyIdList) {
			writeInData(fileName, Long.toString(propertyId));
		}

	}

	public static void readInData(File file, List<Double> priceList, List<Long> propertyIdList) throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader(file));

		String st = "";

		countProperties = 0;

		while ((st = reader.readLine()) != null) {
			System.out.println(st);
			priceList.add(Double.parseDouble(st.split(" ")[2]));
			propertyIdList.add(Long.parseLong(st.split(" ")[0]));
			countProperties++;
		}
	}

	public static void writeInData(String file, String content) throws IOException {

		BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
		try {

			writer.write(content);
			writer.newLine();
			writer.flush();
			System.out.println("Writing done.");
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if (writer != null) {
				writer.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	public static double calculateTotalValue(List<Double> list) {

		double sum = 0;
		for (Double price : list) {
			sum += price;
		}

		return sum;
	}

}
