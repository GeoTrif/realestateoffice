package com.sda.geotrif;

/*
 * Your client is a real estate office manager who needs to run reports on property listings. You will build two applications that will provide two 
different reports. One report is focused on the amount of property an agent is selling. The other report provides an overview of the types of property 
for sale.
Your listings file will contain property number, property type, price, and agent id.
Task:
A.  Develop an application that reads your listings.txt file, analyzes the property listed per agent, and outputs a report to an agentreport.txt file. 
Your application should do the following:
1.  Prompt the user for the name of the input file (listings.txt).
2.  Use an unbuffered file input stream to open listings.txt file and read in property listings.
3.  Store each property type into a Set.
a.  Convert property type to upper case before adding to your Set using method(s) from String class.
b.  Sort your Set of property types alphabetically.
4.  Use a Map to calculate total property listed in dollars and cents for each agent id.
Note: Agent id would be the key, and accumulated total of property listed would be the value.
4.1  Sort your Map by agent id.
4.2  Create an agentreport.txt file.
5.  Use an Iterator to iterate through your Set and write your sorted set of property types sold by the agents to the agentreport.txt file using 
unbuffered file output stream.
6.  Iterate through your Map to write your sorted pair of agent id and total property listed to the agentreport.txt file using unbuffered file output
 stream. 
B.  Develop an application that reads your listings.txt file, analyzes the properties listed, and outputs an overview of properties listed to an 
overview.txt file. Your application should do the following:
1.  Prompt the user for the name of the input file (listings.txt).
2.  Open the listings.txt file and read in property listing information using a buffered FileReader.
3.  Count the total number of property listings for sale.
3.1 Use buffered FileWriter to write the count of the number of property listings to your overview.txt file.
4.  Calculate the total value of property for sale.
4.1 Use a buffered FileWriter to write the total value of properties currently for sale.
5.  Store each property id into an ArrayList.
a.  Sort the ArrayList of property ids using natural ordering.
b.  Use a for-each loop to iterate through the sorted ArrayList and write property ids to overview.txt file using buffered FileWriter.
6.  Use buffered FileWriter to write the total value of the properties listed and the total number of properties currently for sale.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.Map.Entry;

import com.sda.geotrif.model.Listing;
import com.sda.geotrif.util.FileUtil;

public class AgentReportOOP {

	public static double newTotal;

	public static void main(String[] args) throws IOException {

		Set<String> mySet = new TreeSet<>();
		Map<Integer, Double> map = new TreeMap<>();
		List<Listing> listingList = new ArrayList<>();

		File file = FileUtil.readFile();

		readInData(file, mySet, map, listingList);

		for (Listing list : listingList) {

			if (map.containsKey(list.getAgentId())) {
				double newValue = map.get(list.getAgentId()) + list.getPrice();
				map.put(list.getAgentId(), newValue);

			} else {

				map.put(list.getAgentId(), list.getPrice());
			}

		}

		for (Integer keymap : map.keySet()) {
			System.out.println(keymap + "  " + map.get(keymap));
		}

		System.out.println("*************************");

		System.out.println(mySet);

		FileUtil.createAgentReportFile();

		String nameFile = FileUtil.getAgentReportFileName();

		FileUtil.writeToFile(mySet, nameFile, map);

	}

	public static void readInData(File file, Set<String> set, Map<Integer, Double> map, List<Listing> list)
			throws FileNotFoundException {

		Scanner sc = new Scanner(file);

		while (sc.hasNextLine()) {

			String line = sc.nextLine();
			set.add(line.split(" ")[1].toUpperCase());

			String[] arraySplit = line.split(" ");

			Listing listing = new Listing(arraySplit[0], arraySplit[1], Double.parseDouble(arraySplit[2]),
					Integer.parseInt(arraySplit[3]));

			list.add(listing);

		}

	}

}
